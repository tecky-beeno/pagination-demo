console.log('users.js')

let main = document.querySelector('main')
let keyword = document.querySelector('#keyword')
let order = document.querySelector('#order')
let page = 1

async function loadData() {
  main.innerHTML = '<p>Loading...</p>'
  let params = new URLSearchParams()
  params.set('page', page)
  params.set('keyword', keyword.value)
  let res = await fetch(`/users?` + params.toString() + '&' + order.value)
  let data = await res.json()
  console.log('data:', data)
  const { total, users, itemPerPage } = data
  let pages = Math.ceil(total / itemPerPage)
  let hideNext = page >= pages ? 'hidden' : ''
  main.innerHTML = `
  <div class="users">
    <div class="count">
      total: ${total}
    </div>
    <div class="pages">
      ${page} / ${pages}
      <button onclick="nextPage()" ${hideNext}>Next Page</button>
    </div>
  ${users
    .map(
      user => `
    <div class="user">
      #${user.id} ${user.username}
    </div>
  `,
    )
    .join('')}
  </div>
  `
}

loadData()

function nextPage() {
  page++
  loadData()
}

function search() {
  page = 1
  loadData()
}
