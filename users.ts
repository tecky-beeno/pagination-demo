import express from 'express'
import { knex } from './db'

let router = express.Router()

export default router

let itemPerPage = 3
router.get('/users', async (req, res) => {
  let page = (req.query.page as any) || 1

  function makeQuery() {
    let query = knex('users')
    if (req.query.keyword) {
      query = query.where(knex.raw(`username like ?`, `%${req.query.keyword}%`))
    }
    return query
  }

  let { total } = await makeQuery()
    .select(knex.raw(`count(*) as total`))
    .first()

  let users = await makeQuery()
    .select('id', 'username')
    .orderBy(req.query.orderBy as string,req.query.order as any)
    .offset((page - 1) * itemPerPage)
    .limit(itemPerPage)

  res.json({ total, users, itemPerPage })
})
