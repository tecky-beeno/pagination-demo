import { Knex } from 'knex'
import { allNames } from '@beenotung/tslib/constant/character-name'

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex('users').del()

  // Inserts seed entries

  await knex('users').insert(allNames.map(username => ({ username })))
}
